import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import Element from 'element-ui'
import 'element-ui/lib/theme-chalk/index.css'
// 导入字体图标
import './assets/fonts/iconfont.css'
// 导入全局样式
import './assets/css/global.css'
// 导入富文本编译器
import VueQuillEditor from 'vue-quill-editor'
// require styles 导入导入富文本编译器对应的样式
import 'quill/dist/quill.core.css'
import 'quill/dist/quill.snow.css'
import 'quill/dist/quill.bubble.css'

Vue.use(Element)
Vue.use(VueQuillEditor)
Vue.config.productionTip = false
Vue.prototype.bus = new Vue()

// 图片显示接口
// Vue.prototype.$baseApi = 'http://8.138.110.169:90/static/images/'
Vue.prototype.$baseApi = 'http://127.0.0.1:90/static/images/'

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
