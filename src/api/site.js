import { request } from './request'
// 获取相关的学校【模糊搜索】
export function getSchoolByLike(data) {
    return request({
        url: '/site/school',
        method: 'post',
        data
    })
}


// 获取学校对应的建筑列表
export function getAllSchoolPlaceBySchoolId(id) {
    return request({
        url: '/site/school/' + id,
        method: 'get'
    })
}

// 获取地区码表
export function getArea() {
    return request({
        url: '/site/area',
        method: 'get'
    })
}