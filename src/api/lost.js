import { request } from './request'

// 获取所有失物
export function getAllLost(data) {
    return request({
        url: '/lost/findPage',
        method: 'post',
        data
    })
}

// 获取单个失物
export function getLostById(id) {
    return request({
        url: '/lost/' + id,
        method: 'get'
    })
}


// 添加失物
export function addLost(data) {
    return request({
        url: '/lost',
        method: 'post',
        data
    })
}

// 修改失物的审核状态
export function upLostByAudit(id,data) {
    return request({
        url: '/lost/' + id + "/audit",
        method: 'put',
        data
    })
}

// 修改失物的寻回状态
export function upLostByState(id) {
    return request({
        url: '/lost/' + id + "/state",
        method: 'put'
    })
}


// 删除失物
export function delLost(id) {
    return request({
        url: '/lost/' + id,
        method: 'delete'
    })
}