import { request } from './request'

// 获取所有用户
export function getAllUser(data) {
	return request({
		url: '/users/findPage',
		method: 'post',
		data
	})
}

// 根据 用户名称 进行模糊搜索
export function getUserByLike(data) {
	return request({
		url: '/users/name',
		method: 'post',
		data
	})
}

// 获取单个用户
export function getUserById(id) {
	return request({
		url: '/users/' + id,
		method: 'get'
	})
}

// 删除用户
export function delUser(id) {
	return request({
		url: '/users/' + id,
		method: 'delete',
	})
}

// 修改用户
export function updateUser(data) {
	return request({
		url: '/users',
		method: 'put',
		data
	})
}

// 添加用户
export function addUser(data) {
	return request({
		url: '/users',
		method: 'post',
		data
	})
}