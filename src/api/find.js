import { request } from './request'

// 获取所有招领
export function getAllFind(data) {
    return request({
        url: '/find/findPage',
        method: 'post',
        data
    })
}

// 获取单个招领
export function getFindById(id) {
    return request({
        url: '/find/' + id,
        method: 'get'
    })
}


// 添加招领
export function addFind(data) {
    return request({
        url: '/find',
        method: 'post',
        data
    })
}

// 修改失物的寻回状态
export function upFindByState(id,data) {
    return request({
        url: '/find/' + id + "/state",
        method: 'put',
        data
    })
}

// 修改招领的审核状态
export function upFindByAudit(id,data) {
    return request({
        url: '/find/' + id + "/audit",
        method: 'put',
        data
    })
}


// 删除招领
export function delFind(id) {
    return request({
        url: '/find/' + id,
        method: 'delete'
    })
}