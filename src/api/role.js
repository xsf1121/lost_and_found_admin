import { request } from './request'

// 获取该用户下所在学校的角色列表
export function getAllRoleBySchool(userId) {
    return request({
        url: '/role/' + userId,
        method: 'post'
    })
}

// 获取角色列表
export function getAllRole() {
    return request({
        url: '/role',
        method: 'get'
    })
}

// 获取单个角色
export function getRoleById(id) {
    return request({
        url: '/role/' + id,
        method: 'get'
    })
}

// 获取单个角色
export function updateRole(data) {
    return request({
        url: '/role',
        method: 'put',
        data
    })
}

// 获取单个角色
export function addRole(data) {
    return request({
        url: '/role',
        method: 'post',
        data
    })
}

// 删除角色
export function delRole(id) {
    return request({
        url: '/role/' + id,
        method: 'delete'
    })
}