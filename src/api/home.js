import { request } from './request'

// 获取用户近七天新增的数量
export function getNewUser() {
    return request({
        url: '/home/newUser',
        method: 'get'
    })
}

// 获取每个分类底下的物品量
export function getNewClassify() {
    return request({
        url: '/home/newClassify',
        method: 'get'
    })
}

// 获取失物近七天新增的数量
export function getNewArticle() {
    return request({
        url: '/home/newArticle',
        method: 'get'
    })
}