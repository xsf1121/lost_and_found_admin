import { request } from './request'
// 登录
export function getLogin(data) {
	return request({
		url: '/adminUsers/adminLogin',
		method: 'post',
		data
	})
}
