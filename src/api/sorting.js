import { request } from './request'

// 获取所有物品分类
export function getAllSorting() {
    return request({
        url: '/sorting',
        method: 'get'
    })
}

// 获取单个分类
export function getSortingById(id) {
    return request({
        url: '/sorting/' + id,
        method: 'get'
    })
}

// 删除物品分类
export function delSorting(id) {
    return request({
        url: '/sorting/' + id,
        method: 'delete'
    })
}

// 修改物品分类
export function upSorting(data) {
    return request({
        url: '/sorting',
        method: 'put',
        data
    })
}

// 修改物品分类的状态
export function upSortingByStatus(id) {
    return request({
        url: '/sorting/' + id + '/status',
        method: 'put'
    })
}

// 添加分类
export function addSorting(data) {
    return request({
        url: '/sorting',
        method: 'post',
        data
    })
}