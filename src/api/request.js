import axios from "axios";
import ElementUI from 'element-ui';
import Cookies from 'js-cookie'
import router from "@/router";

let loading

// loading开始 方法
function startLoading() {
    // element-ui loading 服务调用方式
    loading = ElementUI.Loading.service({
        lock: true,
        text: '拼命加载中...',
        spinner: 'el-icon-loading',
    })
}

// loading结束 方法
function endLoading() {
    loading.close()
}

export function request(tt) {
    const install = axios.create({
        headers: {'Content-Type': 'multipart/form-data'},
        baseURL: '/api',
        timeout: 60000
    })
    // 请求拦截器
    install.interceptors.request.use(request => {
        // 请求携带token
        if (Cookies.get('AminToken')) {
            request.headers['Authorization'] = Cookies.get('AminToken')
        }
        // startLoading();
        return request
    })
    // 响应拦截器
    install.interceptors.response.use(response => {
            if (response.data.code === 401) {
                ElementUI.Message({
                    // message: '登录过期，请重新登录',
                    message: response.data.msg,
                    type: 'error'
                })
                Cookies.remove('AminToken')
                router.push({path: '/login'}).then(r => {
                    return
                })
            }
            // setTimeout(() => {
            //     endLoading()
            // }, 200);
            return response.data
        }
    ),
        (error) => {
            if (
                axios.isAxiosError(error) &&
                error.code === 'ECONNABORTED' &&
                error.message.includes('timeout')
            ) {
                // 超时错误处理
                ElementUI.Message({
                    message: '请求超时，请检查网络',
                    type: 'error',
                });
            }
            // 其他错误处理
            return Promise.reject(error);
        }
    return install(tt)
}
