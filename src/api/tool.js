import { request } from './request'

// 上传单个图片
export function uploadImg(data) {
    return request({
        url: '/upload',
        method: 'post',
        data
    })
}

// 上传单个图片
export function multipleUpload(data) {
    return request({
        url: '/multiple-upload',
        method: 'post',
        data
    })
}