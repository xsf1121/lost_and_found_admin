import { request } from './request'

// 获取所有管理员
export function getAllAdmin(data) {
    return request({
        url: '/adminUsers/findPage',
        method: 'post',
        data
    })
}

// 根据 管理员名称 进行模糊搜索
export function getAdminByLike(data) {
    return request({
        url: '/adminUsers/name',
        method: 'post',
        data
    })
}

// 删除管理员
export function delAdmin(id) {
    return request({
        url: '/adminUsers/' + id,
        method: 'delete'
    })
}

// 修改管理员
export function upAdmin(data) {
    return request({
        url: '/adminUsers',
        method: 'put',
        data
    })
}

// 添加管理员
export function addAdmin(data) {
    return request({
        url: '/adminUsers',
        method: 'post',
        data
    })
}

// 获取单个管理员
export function getAdminById(id) {
    return request({
        url: '/adminUsers/' + id,
        method: 'get'
    })
}