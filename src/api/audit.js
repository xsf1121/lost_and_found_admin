import { request } from './request'

// 获取所有失物[待审核]
export function getAllLostByAudit(data) {
    return request({
        url: '/audit/lostFindPage',
        method: 'post',
        data
    })
}

// 获取所有招领[待审核]
export function getAllFindByAudit(data) {
    return request({
        url: '/audit/findFindPage',
        method: 'post',
        data
    })
}