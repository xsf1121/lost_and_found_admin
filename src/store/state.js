const state = {
    menu: [{
            index: '1',
            title: '账户管理',
            content: [{ item: '管理员管理', path: '/admin',icon: 'el-icon-cpu' },{ item: '用户管理', path: '/user',icon: 'el-icon-connection' }],
        },
        {
            index: '2',
            title: '分类管理',
            content: [{ item: '物品分类', path: '/sorting',icon: 'el-icon-monitor' },{ item: '角色分类', path: '/role',icon: 'el-icon-coordinate' }],
        },
        {
            index: '3',
            title: '物品管理',
            content: [{ item: '失物管理', path: '/lost',icon: 'el-icon-mouse' }, { item: '招领管理', path: '/find',icon: 'el-icon-thumb' }],
        },
        {
            index: '4',
            title: '审核',
            content: [{ item: '待审核 [失物]', path: '/auditLost',icon: 'el-icon-folder-add' },{ item: '待审核 [招领]', path: '/auditFind',icon: 'el-icon-folder-remove\n' }],
        }
    ],
}
export default state