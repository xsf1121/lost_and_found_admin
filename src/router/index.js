import Vue from 'vue'
import VueRouter from 'vue-router'

Vue.use(VueRouter)

const routes = [
    {path: '/', redirect: '/login'},
    {
        path: '/login',
        name: '登录页面',
        component: () =>
            import ('@/views/login/index')
    },
    {
        path: '/home',
        component: () =>
            import ('@/views/home/index'),
        name: '首页',
        redirect: '/homemain',
        children: [
            {
                path: '/homemain',
                component: () =>
                    import ('@/components/home/HomeMain')
            },
            {
                path: '/admin',
                name: "管理员管理页面",
                component: () =>
                    import ('@/views/admin/index.vue'),
            },
            {
                path: '/user',
                name: "用户管理页面",
                component: () =>
                    import ('@/views/user/index.vue'),
            },
            {
                path: '/sorting',
                name: "物品分类",
                component: () =>
                    import ('@/views/sorting/index.vue'),
            },
            {
                path: '/role',
                name: "分类管理",
                component: () =>
                    import ('@/views/role/index.vue'),
            },
            {
                path: '/lost',
                name: "失物管理",
                component: () =>
                    import ('@/views/lost/index.vue'),
            },
            {
                path: '/find',
                name: "招领管理",
                component: () =>
                    import ('@/views/find/index.vue'),
            },
            {
                path: '/auditLost',
                name: "待审核 [失物]",
                component: () =>
                    import ('@/views/audit/lost.vue'),
            },
            {
                path: '/auditFind',
                name: "待审核 [招领]",
                component: () =>
                    import ('@/views/audit/find.vue'),
            }
        ]
    }
]

const router = new VueRouter({
    routes
})
// 解决代码冗余
const originalPush = VueRouter.prototype.push
VueRouter.prototype.push = function push(location) {
    return originalPush.call(this, location).catch(err => err)
}
const originalReplace = VueRouter.prototype.replace
VueRouter.prototype.replace = function replace(location) {
    return originalReplace.call(this, location).catch(err => err)
}

router.beforeEach((to, from, next) => {
    if (to.path === '/login') return next();
    next();
})

export default router
