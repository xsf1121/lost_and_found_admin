const { defineConfig } = require('@vue/cli-service')

module.exports = defineConfig({
  transpileDependencies: true,
  devServer: {
    host: '0.0.0.0',
    port:3002,
    allowedHosts: "all",
    client: {
      webSocketURL: 'ws://0.0.0.0:3002/ws',
    },
    proxy: {
      '/api': {
        // target: 'http://127.0.0.1:90',
        target: 'http://8.138.110.169:90',
        changeOrigin: true,
        pathRewrite: {       // 路径重写
          "^/api": ''
        }
      }
    },
    headers: {
      'Access-Control-Allow-Origin': '*',
    }
  }
})